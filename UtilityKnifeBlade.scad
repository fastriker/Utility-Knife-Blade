long_side = 60;
short_side = 30;
height = 18.6;
gap_diameter = 3.5;
gap_depth = 4.5;
gap_distance = 3;
thickness = 0.6;
edge = 2;

module utility_knife_blade_two_dimensions()
{
    difference()
    {
        polygon([[-long_side/2, height], [-    short_side/2, 0], [short_side/2, 0], [long_side/2, height]]);
        for(i = [-1, 1])
        {
            translate([-gap_diameter/2+i*(gap_distance/2+gap_diameter/2), 0])
            {
                square([gap_diameter, gap_depth-gap_diameter/2]);
                translate([gap_diameter/2, gap_depth-gap_diameter/2])
                {
                    $fn = 30;
                    circle(d=gap_diameter);
                }
            }
        }
    }
}

module utility_knife_blade()
{
    difference()
    {
        translate([0, 0, -thickness/2])
        {
            linear_extrude(thickness)
            {
                utility_knife_blade_two_dimensions();
            }
        }
        
        length = sqrt(pow(edge, 2)+pow(thickness,2));
        a = atan((thickness/2)/edge);
        echo(a);
        translate([-long_side/2, height, 0])
        {
            for(i = [-1, 1])
            {
                rotate([-i*a, 0, 0])
                {
                    translate([0, -length, -thickness/4+i*thickness/4])
                    {
                        cube([long_side, length,thickness/2]);
                    }
                }
            }
        }
    }
}

utility_knife_blade();